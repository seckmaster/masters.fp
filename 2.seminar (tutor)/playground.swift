
let xs = [1, 2, 5, 11, 9] 
let target = 20

var mapping = [Int: Int]()

var i = 0
while i < xs.count {
	let el = xs[i]
	if let index = mapping[el] {
		print(index, i, xs[index] + el == target)
		break
	} else {
		mapping[target - el] = i
	}
	i += 1
}