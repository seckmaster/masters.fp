(* Definiraj podatkovni tip za seznam, v katerem 
lahko hranimo elemente dveh različnih tipov 'a in 'b 
in kjer lahko seznam zapišemo kot:

- Prvi 3 || Drugi false || Drugi true || Prvi 1 || konec;
val it = Prvi 3 || Drugi false || Drugi true || Prvi 1 || konec : (int,bool) seznam *)

datatype ('a, 'b) prvi_or_drugi = 
	

datatype ('a, 'b) list_t = 
	konec 
	| || of (('a, 'b) list_t * ('a, 'b) list_t)
	| Prvi of 'a 
	| Drugi of 'b;

infixr 5 ||;

Prvi 3 || Drugi false || Drugi true || Prvi 1 || konec;

(* Druga naloga: naredi seznam, podoben zgornjemu, 
vendar pa da si morajo v njem elementi, narejeni s konstruktorjem 
Prvi in Drugi slediti izmenično. *)

datat(*ype 'a Prvi_t = 
	konec
	| Prvi of 'a;

datatype 'b Drugi_t = 
	konec
	| Drugi of 'b;

datatype ('a, 'b) prvi_than_drugi = 
	konec
	| Prvi of (('a Prvi_t) * ('b Drugi_t))
	| Drugi of (('b Drugi_t) * ('a Prvi_t));

datatype ('a, 'b) list_t = 
	konec 
	| || of (Prvi 'a * ('a, 'b) list_t);


infixr 5 ||;

Prvi 3 || Drugi false || konec;*)







