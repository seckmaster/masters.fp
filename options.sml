(* poiscemo prvo lokacijo pojavitve elementa el *)
(* (int list * int) -> int option *)
fun najdi(sez: int list, el: int) =
	if null sez then NONE
	else if hd sez = el then SOME 0
	else let val rez = najdi(tl sez, el) in
		if isSome rez then 
			SOME (1 + valOf rez)
		else 
			NONE
		end;

najdi([1, 2, 3, 4, 5], 0);
najdi([1, 2, 3, 4, 5], 1);
najdi([1, 2, 3, 4, 5], 2);
najdi([1, 2, 3, 4, 5], 3);
najdi([1, 2, 3, 4, 5], 4);
najdi([1, 2, 3, 4, 5], 5);