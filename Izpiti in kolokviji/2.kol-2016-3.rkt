#lang racket

; 2.

(define (limit tok n)
  (cond [(> n 0) (begin
                   (displayln (car tok))
                   (limit ((cdr tok)) (- n 1)))]))

(define (naravna)
  (define (promise el)
    (cons el (lambda () (promise (+ el 1)))))
  (promise 1))

(define (tok120)
  (define map (list (cons 0 1) (cons 1 2) (cons 2 0)))
  (define (promise el)
    (cons el (lambda () (promise (cdr (assoc el map))))))
  (promise 1))

(define (preskocni tok skoki)
  (define (preskoci t n)
    (cond [(> n 0) (preskoci ((cdr t)) (- n 1))]
          [#t t]))
  (define (promise tok skoki)
    (cons (car tok) 
          (lambda () (promise (preskoci ((cdr tok)) (car skoki)) ((cdr skoki))))))
  (promise tok skoki))

(limit (preskocni (naravna) (tok120)) 10)
(displayln "")

; 3.

(define (cache f)
  (mcons null f))

(define (exec c input)
  (define (calculate)
    (apply (mcdr c) input))
  (define (store res) 
    (set-mcar! c (cons (cons input res) (mcar c)))
    res)
  (let ([result (assoc input (mcar c))])
    (if result
        (cdr result)
        (store (calculate)))))

(define (vsota a b c) (+ a b c))
(define c (cache vsota))
c
(exec c (list 2 4 7))
c
(exec c (list 1 2 3))
c
(exec c (list 2 4 7))
c