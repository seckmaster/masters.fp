fun f1 (a, b, c::d) [i, j] =
	if c then fn a => b (SOME i)
	else fn b => a (j + 1);

val u = 1;
fun f v = fn w => u + v + w;
val rez1 = f 5 6;
val u = 3;
val rez2 = f 5 6;

datatype ('a, 'b) chain = 
	final
	| Node of ({a: 'a ref, b: 'b} * ('b, 'a) chain);

fun chain_to_list x =
	let
		fun chain1 final acc1 acc2 = (acc1, acc2)
			| chain1 (Node ({a=a, b=b}, x)) acc1 acc2 = chain2 x (acc1 @ [!a]) (acc2 @ [b])
		and
			chain2 final acc1 acc2 = (acc1, acc2)
				| chain2 (Node ({a=a, b=b}, x)) acc1 acc2 = chain1 x (acc1 @ [b]) (acc2 @ [!a])
	in
		chain1 x [] []
	end;

val izraz = Node ({a=ref 15, b="pon"}, 
	Node ({a=ref "tor", b=12},
		Node ({a=ref 42, b="sre"},
			Node ({a=ref "cet", b=314},
				final))));

chain_to_list izraz;

fun first_op sez =
	case sez of 
		[] => [true]
		| g::r => (not g)::(second_op r)
and second_op sez =
	case sez of 
		[] => [false]
		| g::r => (g)::(third_op r)
and third_op sez =
	case sez of 
		[] => nil
		| g::r => (true)::(first_op r);

first_op [true];
first_op [true, false];
first_op [true, true, true];
first_op [true, true, true, false];
first_op [true, false, true, true, true];
first_op [true, true, false, true, false, true];

fun op123 n1 h1 n2 h2 n3 h3 sez =
	case sez of 
		g1::g2::g3::t => (h1 g1)::(h2 g2)::(h3 g3)::(op123 n1 h1 n2 h2 n3 h3 t)
		| g1::g2::[] => (h1 g1)::(h2 g2)::(n3 ())
		| g1::[] => (h1 g1)::(n2 ())
		| [] => n1 ();


val f = op123 (fn _ => [true])
              (fn h => not h)
              (fn _ => [false])
              (fn h => h)
              (fn _ => nil)
              (fn _ => true);


f [true];
f [true, false];
f [true, true, true];
f [true, true, true, false];
f [true, false, true, true, true];
f [true, true, false, true, false, true];
