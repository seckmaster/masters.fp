#lang racket

; misc

(define (limit tok n)
  (cond [(> n 0) 
         (begin 
           (displayln (car tok))
           (limit ((cdr tok)) (- n 1)))]))

; 1.

; 2.

(define (fib)
  (define (promise a b)
    (cons a (lambda () 
              (promise b (+ a b)))))
  (promise 1 1))

(limit (fib) 10)

; 3.

(struct konst (int) #:transparent)
(struct bool (b) #:transparent)
(struct negiraj (e) #:transparent)
(struct sestej (e1 e2) #:transparent)
(struct ce-potem-sicer (pogoj res nires) #:transparent)
(struct uredi (e1 e2) #:transparent)
(struct urejeno (k1 k2) #:transparent)

(define (jais e)
  (cond [(konst? e) e]
        [(bool? e) (if (boolean? (bool-b e)) e (error "pričakujem boolean"))]
        [(negiraj? e) (let ([v (jais (negiraj-e e))]) 
                        (cond [(konst? v) (konst (- 0 (konst-int v)))]
                              [(bool? v) (bool (not (bool-b v)))]
                              [#t (error "sintaksa ni pravilna")]))]
        [(sestej? e) (let ([v1 (jais (sestej-e1 e))]
                           [v2 (jais (sestej-e2 e))])
                       (if (and (konst? v1) (konst? v2))
                           (konst (+ (konst-int v1) (konst-int v2)))
                           (error "sintaksa ni pravilna")))]
        [(ce-potem-sicer? e) (if (bool? (ce-potem-sicer-pogoj e))
                                 (if (bool-b (ce-potem-sicer-pogoj e))
                                     (jais (ce-potem-sicer-res e))
                                     (jais (ce-potem-sicer-nires e)))
                                 (error "ni bool"))]
        [(uredi? e) (let ([v1 (jais (uredi-e1 e))]
                          [v2 (jais (uredi-e2 e))])
                      (if (and (konst? v1) (konst? v2))
                          (let ([k1 (konst-int v1)]
                                [k2 (konst-int v2)])
                            (urejeno (konst (min k1 k2)) (konst (max k1 k2))))
                          (error "ni konstanta")))]
        [#t (error "sintaksa izraza ni pravilna")]))

(jais (negiraj (konst 10)))
(jais (negiraj (konst -10)))
(jais (negiraj (bool true)))
(jais (negiraj (bool false)))
(jais (sestej (negiraj (konst 10)) (sestej (konst 5) (konst 2))))
(jais (negiraj (sestej (konst 5) (konst 10))))
(jais (ce-potem-sicer (bool #t) (konst 10) (sestej (konst 1) (konst 2))))
(jais (ce-potem-sicer (bool #f) (konst 10) (sestej (konst 1) (konst 2))))
(jais (uredi (konst 10) (konst 20)))
(jais (uredi (konst 20) (konst 10)))
(displayln "")
; (jais (uredi (konst 20) (bool false)))
; (jais (uredi (bool false) (konst 10)))

; 4.

; a. pri fib se podproblemi prekrivajo:
; (fib 8)
;    => (fib 7) + (fib 6)
;       (fib 7)
;          => (fib 6) (fib 5) // (fib 6) se računa mnogo-krat
;    pri potenci prekrivanja ni:
; (potenca 2 4)
;    => 2 * (potenca 2 3) ... (2 * 2 * 2 * 2)

; b. 

(define (potenca [osnova 2] #:exp [exp 10]) 
  (if (= exp 0)
      1
      (* osnova (potenca osnova #:exp (- exp 1)))))

(potenca) ; 1024
(potenca 3) ; 59049
(potenca #:exp 5) ; 32
(potenca 4 #:exp 2) ; 16
(potenca #:exp 4 4) ; 256