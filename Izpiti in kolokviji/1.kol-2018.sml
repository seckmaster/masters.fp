fun f ({g=g,h=h}, [i,j]) k = if valOf i
	then fn x => g (k+x)
	else fn x => h (k-x) ^ "nil";

datatype pot = 
	Left of pot 
	| Right of pot
	| Up of pot
	| Down of pot
	| start;

fun coordinate pot =
	let
		fun coordinate pot {x=x, y=y} =
			case pot of
				Left pot => coordinate pot {x = x - 1, y = y}
				| Right pot => coordinate pot {x = x + 1, y = y}
				| Up pot => coordinate pot {x = x, y = y + 1}
				| Down pot => coordinate pot {x = x, y = y - 1}
				| start => {x = x, y = y}
	in
		coordinate pot {x = 0, y = 0}
	end;
 
coordinate (Left (Up (Up (Left (Down (Right start))))));

fun f1 c l1 l2 = if c then SOME (l1, l2) else NONE;
fun f2 c l1 = if c then l1 else 0;
fun f3 c l1 = if not c then SOME l1 else NONE;

fun general c l1 p t f = if p c then t l1 else f l1;

fun f1short c l1 l2 = general c l1 (fn c => c) (fn l1 => SOME (l1, l2)) (fn _ => NONE);
f1 true 10 20 = f1short true 10 20;
f1 false 10 20 = f1short false 10 20;

fun f2short c l1 = general c l1 (fn c => c) (fn l1 => l1) (fn l2 => 0);
f2 true 10 = f2short true 10;
f2 false 10 = f2short false 10;

fun f3short c l1 = general c l1 (fn c => not c) (fn l1 => SOME l1) (fn _ => NONE);
f3 true 10 = f3short true 10;
f3 false 10 = f3short false 10;

fun filter f xs =
	List.foldr (fn (el, acc) => if f el then el::acc else acc) [] xs;

filter (fn x => x mod 2 = 0) [1, 2, 3, 4, 5, 6, 7, 8];

(* ---- *)

structure Stack = struct
 	exception StackEmpty

 	val stack: int list ref = ref [] 

 	fun push x =
		stack := (x::(!stack))
	
	fun pop () =
		case !stack of
			[] => raise StackEmpty
			| g::r => (stack := r; g)
	
	fun isEmpty () =
		!stack = []
end;

Stack.push 10;
Stack.push 20;
(* ... another programmer does this ... *)
Stack.stack := [];
(* ... *)
(*Stack.pop ();*) (* I expect 20, but get exception :( *)

signature STACK = 
sig
	exception StackEmpty
	val push: int -> unit
	val pop: unit -> int
	val isEmpty: unit -> bool
end;

structure Stack :> STACK =
struct
 	exception StackEmpty

 	val stack: int list ref = ref [] 

 	fun push x =
		stack := (x::(!stack))
	
	fun pop () =
		case !stack of
			[] => raise StackEmpty
			| g::r => (stack := r; g)
	
	fun isEmpty () =
		!stack = []
end;

Stack.push 10;
Stack.push 20;
(* ... not possible, because `stack` is not _private_ ... *)
(*Stack.stack := [];*)
(* ... *)
(*Stack.pop ();*) (* I expect 20, but get exception :( *)

datatype sequenceA = A of (int * sequenceB)
	and sequenceB = B of (int * sequenceB) | C of sequenceA | fin;

val exam1 = A (3, B(5, B(4, C(A (3, B(1, B(2, fin)))))));

fun checkA (A (v, seqB)) = checkB seqB 0 v
and checkB fin sum v = sum = v
	| checkB (B (v2, seqB)) sum v = checkB seqB (sum + v2) v
	| checkB (C seqA) sum v = sum = v andalso checkA seqA;

checkA (A (3, B(5, B(4, C(A (3, B(1, B(2, fin))))))));
checkA (A (3, B(1, B(2, C(A (15, B(5, B(10, fin))))))));
checkA (A (3, B(1, B(2, C(A (14, B(5, B(10, fin))))))));
checkA (A (4, B(1, B(2, C(A (15, B(5, B(10, fin))))))));