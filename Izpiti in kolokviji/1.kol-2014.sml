fun preveri a b c = 
	case a of 
		[] => []
		| ga::ra => case b of 
			[] => []
			| gb::rb => case c of 
				[] => []
				| gc::rc =>
					if ga + gb + gc = 3 then 111::preveri ra rb rc
					else if ga = 1 orelse gb = 1 orelse gc = 1 then 1::preveri ra rb rc
					else 0::preveri ra rb rc;

preveri [0,0,0,0,0,1] [1,0,0,0,0,1] [1,0,1,0,0,1];
preveri [] [] [];
preveri [] [1] [1];
preveri [1] [] [1];
preveri [1] [] [0];

fun preveri (ga::ra) (gb::rb) (gc::rc) = 
	if ga + gb + gc = 3 then 111::preveri ra rb rc 
	else if ga = 1 orelse gb = 1 orelse gc = 1 then 1::preveri ra rb rc
	else 0::preveri ra rb rc
	| preveri _ _ _ = [];

fun preveri a b c =
    case (a, b, c) of 
        (1::at, 1::bt, 1::ct) => 111 :: preveri at bt ct
      | (1::at, _::bt, _::ct) => 1 :: preveri at bt ct
      | (_::at, 1::bt, _::ct) => 1 :: preveri at bt ct
      | (_::at, _::bt, 1::ct) => 1 :: preveri at bt ct
      | (0::at, 0::bt, 0::ct) => 0 :: preveri at bt ct
      | (_, _, _) => [];

preveri [0,0,0,0,0,1] [1,0,0,0,0,1] [1,0,1,0,0,1];
preveri [0] [1] [1];
preveri [] [] [];
preveri [] [1] [1];
preveri [1] [] [1];
preveri [1] [] [0];

fun twist seznam = 
	case seznam of
		[] => []
		| [a] => [a]
		| prvi::drugi::rep => drugi::prvi::(twist rep);

twist [1,2,3,4,5,6];

fun twist xs =
	let 
		fun twist (g1::g2::r, acc) = twist (r, acc @ [g2, g1])
			| twist ([x], acc) = acc @ [x]
			| twist ([], acc) = acc
	in
		twist (xs, [])
	end;

twist [1,2,3,4,5];
twist [1,2,3,4,5,6];

val sodilihi = List.foldl 
	(fn (x, (sodi, lihi)) => if x mod 2 = 0 then (sodi + 1, lihi) else (sodi, lihi + 1)) (0, 0);

sodilihi [1,2,3,4,5,6,7,8];
sodilihi [1,2,3,4,5,5,5,5];

fun f1 x y = [x,y];
fun f2 x y = (x,y);
fun f3 x y = {x=x, y=y};

val x = f1 1;
val x = f2 1;
val x = f3 1;
(*val x = f1 1 "a";  elementi v seznamu morajo biti istega tipa (dobljeno int in string) *)
val x = f2 1 "a";
val x = f3 1 "a";

OS.Process.exit(OS.process.success);
