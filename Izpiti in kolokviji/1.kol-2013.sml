val x = 10;

fun f() = x;

f();
val x = 20;
f();

fun f1 x y z = (x andalso y, z);
fun f2 x y z = (x andalso z, y);
fun f3 x y z = (y andalso z, x);

val x = f1 true; (* value restriction *)
val x = f1 true true;  (* value restriction *)
val x = f1 true true true;
val x = f2 true;  (* value restriction *)
val x = f2 true true;
val x = f2 true true true;
val x = f3 true;
val x = f3 true true;
val x = f3 true true true;

fun f (a,b) c =
    let val (b1,b2) = b
    in if a > 3 andalso b2
	then c b1
	else c b2 end;

exception NapakaDolzine;

fun obdelaj m b =
	let
		fun sestej sez = case sez of [] => 0 | g::r => g + sestej r
		fun pomozna m b acc =
			case (m, b) of
				(g::r, true::rb) => pomozna r rb (acc @ [sestej g])
				| (g::r, false::rb) => pomozna r rb acc
				| ([], []) => acc
				| (_, _) => raise NapakaDolzine
	in
		pomozna m b []
	end;

val m = [[1,2,1],
[0,1,3],
[1,1,0]];
val b = [true, false, true];
obdelaj m b;



signature LogSig = sig
	type vrednost
	val izdelaj : bool * int -> vrednost
	val obdelaj : vrednost -> vrednost 
end;

structure Logika :> LogSig = struct
	type vrednost = bool * int
	exception Napaka
	fun izdelaj (x,y) = if y < 10 then (x,y) else raise Napaka 
	fun obdelaj (x,y) = (not x, if y mod 2 = 0 then y-1 else y+1)
end;

OS.Process.exit(OS.process.success);
