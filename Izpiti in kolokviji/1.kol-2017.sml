fun nekaj a b c d e f = a (b c) (d e);



fun izloci sez = case sez of
	[] => []
	| x::xs => (x,xs)::List.map (fn (a, xs) => (a, x::xs)) (izloci xs);

izloci([1,4,6,7]);

(*a. *) (* (a, x::xs)) *)

(*b. *) (* Ni.  *)

(*c. *)
val x = izloci [1];
val x = izloci [];

datatype prvi = X of int 
				| Y of prvi 
				| Z of drugi 
	 and drugi = W of prvi 
				| tretji
datatype rezultat = P | D;

fun preveri_prvi (X _) = P
	| preveri_prvi (Y x) = preveri_prvi x
	| preveri_prvi (Z x) = preveri_drugi x
and preveri_drugi tretji = D
	| preveri_drugi (W x) = preveri_prvi x;

preveri_prvi (Z (W (Z (W (X 3)))));
preveri_prvi (Z (W (Y (Z tretji))));
preveri_drugi (W (Y (Z tretji)));


signature izraz1 = sig
	type prvi
	type drugi
	datatype rezultat = D | P
	val preveri_prvi : prvi -> rezultat 
end;

signature izraz2 = sig
	datatype prvi = X of int | Y of prvi | Z of drugi 
	datatype drugi = W of prvi | tretji
	datatype rezultat = D | P
	val preveri_prvi : prvi -> rezultat
	val preveri_drugi : drugi -> rezultat 
end;

signature izraz3 = sig
	type prvi
	type drugi
	type rezultat
	val X : int -> prvi
	val W : prvi -> drugi
	val Z : drugi -> prvi
	val preveri_prvi : prvi -> rezultat 
	val preveri_drugi : drugi -> rezultat
end;

signature izraz3 = sig
	type prvi
	type drugi
	type rezultat
	val X : int -> prvi
	val W : prvi -> drugi
	val Z : drugi -> prvi
	val preveri_prvi : prvi -> rezultat 
	val preveri_drugi : drugi -> rezultat
end;

structure izrazi :> izraz3 = 
struct
	datatype prvi = X of int 
				| Y of prvi 
				| Z of drugi 
	 and drugi = W of prvi 
				| tretji
	datatype rezultat = D | P;

	fun preveri_prvi (X _) = P
		| preveri_prvi (Y x) = preveri_prvi x
		| preveri_prvi (Z x) = preveri_drugi x
	and preveri_drugi tretji = D
		| preveri_drugi (W x) = preveri_prvi x;
end;

izrazi.preveri_prvi (izrazi.Z (izrazi.W (izrazi.X 3)));