val student = {ime = "Janez", priimek = "Novak", ocene = [("angl", 8), ("slo", 10), ("FP", 5)], starost = 22};
#ocene student;
#ime student;
(#ime student) ^ " je star " ^ Int.toString(#starost student) ^ " let.";

fun izpis_studenta (student: {ime: string, priimek: string, ocene: (string*int) list, starost: int}) =
 (#ime student) ^ " je star " ^ Int.toString(#starost student) ^ " let.";

izpis_studenta student;

type student_t = {ime: string, priimek: string, ocene: (string*int) list, starost: int};

fun izpis_studenta(student: student_t) = 
	let fun izpis_ocen(ocene: (string*int) list) =
		if null ocene then ""
		else #1 (hd ocene) ^ ": " ^ Int.toString(#2 (hd ocene)) ^ "; " ^ izpis_ocen (tl ocene)
	in
	#ime student ^ " je star " ^ Int.toString(#starost student) ^ " let. Njegove ocene so: " ^ izpis_ocen(#ocene student)
	end

(* ******************* Lastni podatkovni tipi ***************** *)

datatype prevozno_sredstvo_t = 
	Bus of int
	| Avto of string*string
	| Pes;

val a = Avto(("Pes", "Honda"));
a;
val a = Pes;

fun obdelaj_prevoz sredstvo = 
	case sredstvo of 
		Bus i => i
		| Avto i => String.size(#1 i) + String.size(#2 i)
		| Pes => 0;

obdelaj_prevoz(Avto("Honda", "Civic"));
obdelaj_prevoz(Avto("VW", "Golf"));
obdelaj_prevoz(Bus(10));
obdelaj_prevoz(Bus(20));
obdelaj_prevoz(Pes);

datatype izraz = 
	Konstanta of int
	| Negiraj of izraz
	| Plus of izraz * izraz
	| Minus of izraz * izraz
	| Krat of izraz * izraz
	| Deljeno of izraz * izraz
	| Ostanek of izraz * izraz;

val k = Konstanta(10);
val neg = Negiraj(Konstanta 10);
val expr = Plus(Konstanta 10, Minus(Konstanta 20, Konstanta 10));

(* ******************* Naloge: aritmetični izrazi ***************** *)

fun eval expr =
	case expr of
		Konstanta i => real(i)
		| Negiraj i => ~(eval i)
		| Plus (e1, e2) => (eval e1) + (eval e2)
		| Minus (e1, e2) => (eval e1) - (eval e2)
		| Krat (e1, e2) => (eval e1) * (eval e2)
		| Deljeno (e1, e2) => (eval e1) / (eval e2)
		| Ostanek (e1, e2) => real(round(eval e1) mod round(eval e2));
eval k;
eval neg;
eval expr;

fun count_neg expr =
	case expr of
		Konstanta i => 0
		| Negiraj i => 1 + count_neg(i)
		| Plus (e1, e2) => (count_neg e1) + (count_neg e2)
		| Minus (e1, e2) => (count_neg e1) + (count_neg e2)
		| Krat (e1, e2) => (count_neg e1) + (count_neg e2)
		| Deljeno (e1, e2) => (count_neg e1) + (count_neg e2)
		| Ostanek (e1, e2) => count_neg e1 + (count_neg e2);

count_neg(Konstanta 0);
count_neg(Negiraj(Konstanta 1));
count_neg(Negiraj(Negiraj(Negiraj(Plus(Konstanta 10, Negiraj(Konstanta 10))))));

fun max_constant(expr): int =
let 
	fun max(a, b) =
		if a > b then a else b
in
	case expr of
		Konstanta i => i
		| Negiraj i => ~(max_constant i)
		| Plus (e1, e2) => max(max_constant(e1), max_constant(e2))
		| Minus (e1, e2) => max(max_constant(e1), max_constant(e2))
		| Krat (e1, e2) => max(max_constant(e1), max_constant(e2))
		| Deljeno (e1, e2) => max(max_constant(e1), max_constant(e2))
		| Ostanek (e1, e2) => max(max_constant(e1), max_constant(e2))
end;
			
max_constant(Konstanta 10);
max_constant(Konstanta ~10);
max_constant(Plus(Konstanta 10, Konstanta ~20));
max_constant(Plus(Konstanta 10, Deljeno(Ostanek(Konstanta 55, Konstanta ~100), Konstanta ~20)));

fun count_mod_zero(expr): int = 
	case expr of
		Konstanta i => 0
		| Negiraj i => count_mod_zero i
		| Plus (e1, e2) => (count_mod_zero e1) + (count_mod_zero e2)
		| Minus (e1, e2) => (count_mod_zero e1) + (count_mod_zero e2)
		| Krat (e1, e2) => (count_mod_zero e1) + (count_mod_zero e2)
		| Deljeno (e1, e2) => (count_mod_zero e1) + (count_mod_zero e2)
		| Ostanek (e1, e2) => if Real.==(eval(Ostanek(e1, e2)), 0.0) then 1 else 0;

count_mod_zero(Konstanta 10);
count_mod_zero(Ostanek(Konstanta 10, Konstanta 3));
count_mod_zero(Ostanek(Konstanta 10, Konstanta 2));
count_mod_zero(Plus(Ostanek(Konstanta 10, Konstanta 2), Deljeno(Konstanta 200, Ostanek(Konstanta 10, Konstanta 2))));

(* ******************* Polimorfizem podatkovnih tipov ***************** *)

fun hd xs =
	case xs of
		nil => NONE
		| a::b => SOME a;

hd [1, 2, 3];
hd [];

fun tl xs =
	case xs of
		nil => NONE
		| a::b => SOME b;

tl [1, 2, 3];
tl [];

fun valueOf x =
	case x of 
		NONE => raise Empty
		| SOME x => x;

(* valueOf NONE; *) (* Exception *)
valueOf(SOME 10);
valueOf(SOME "Test");

fun isSome x =
	case x of
		SOME x => true
		| NONE => false;

isSome NONE;
isSome (SOME 10);
isSome (SOME "123");

datatype ('a, 'b) list_t = 
	element_a of 'a * ('a, 'b) list_t
	| element_b of 'b * ('a, 'b) list_t
	| eol;

element_a(10, element_a(20, element_b("test", eol)));




