(*	case expr of 
		True => 
		| False => 
		| Variable var => 
		| Equiv (left_expr, right_expr) => 
		| Implies (left_expr, right_expr) =>
		| And expressions => 
		| Or expressions =>
		| Not expr =>*)

Control.Print.printDepth := 100;
Control.Print.printLength := 300;

exception InvalidVariable;
exception InsufficientVariables;

(* Helper functions *)

infix 3 -->  (* implication *)
fun true --> false = false
	| _ --> _ = true;

true --> false = false;
true --> true = true;
false --> false = true;
false --> true = true;

fun contains [] el = false
	| contains (h::tail) el = if h = el then true else contains tail el;

fun all_true [] f = true
	| all_true (h::tail) f = f h andalso all_true tail f;

fun exists_true [] f = false
	| exists_true (h::tail) f = f h orelse exists_true tail f;

fun filter [] f = []
	| filter (h::tail) f = if f h then h::(filter tail f) else filter tail f;

all_true [] (fn _ => true) = true;
all_true [true, true, true] (fn x => x) = true;
all_true [true, false, true] (fn x => x) = false;

exists_true [] (fn _ => true) = false;
exists_true [false, false, false] (fn x => x) = false;
exists_true [false, true, false] (fn x => x) = true;

fun compactMap ((SOME v)::tail) = v::(compactMap tail)
	| compactMap (NONE::tail) = compactMap tail
	| compactMap [] = [];

compactMap [SOME true, NONE, NONE, SOME false, SOME false] = [true, false, false];

fun mapNone f (SOME x) = SOME x
	| mapNone f NONE = f ();
mapNone (fn () => SOME 100) (SOME 10) = SOME 10;
mapNone (fn () => SOME 100) NONE = SOME 100;

(* flatMaps into a reverse order, but that's okay for now *)
fun flatMap f xs =
	let
		fun flatMap ((head::tail)::tailOuter) acc = flatMap (tail::tailOuter) (f head::acc)
			| flatMap ([]::tail) acc = flatMap tail acc
			| flatMap [] acc = acc
	in
		flatMap xs []
	end;

fun id x = x;
flatMap id [[1], [2, 3], [4, 5, 6]] = [6, 5, 4, 3, 2, 1];

fun kleisliMap _ acc [] = ([], acc)
	| kleisliMap f acc (hd::tail) = let
		val (res, acc) = f acc hd
		val (xs, acc) = kleisliMap f acc tail
	in
		(res::xs, acc)
	end;

infixr 0 $ (* https://typeclasses.com/featured/dollar *)
fun f $ arg = f arg;



(* Implementation *)

datatype ''a expression = 
	Not of ''a expression
	| Or of ''a expression list
	| And of ''a expression list
	| Implies of ''a expression * ''a expression
	| Equiv of ''a expression * ''a expression
	| Variable of ''a
	| True
	| False;

(* for debugging purposes *)
fun toString expr =
	let
		fun createIndent 0 = ""
			| createIndent indent = " " ^ (createIndent $ indent - 1)
		fun withIndent indent str = createIndent indent ^ str
		fun toString expr indent =
			case expr of 
				True => withIndent indent "true"
				| False => withIndent indent "false"
				| Variable var => withIndent indent "variable " ^ var
				| Equiv (left_expr, right_expr) => 
					withIndent indent "Equiv\n" ^ (toString left_expr (indent + 2)) ^ "\n" ^ (toString right_expr (indent + 2))
				| Implies (left_expr, right_expr) =>
					withIndent indent "Implies\n" ^ (toString left_expr (indent + 2)) ^ "\n" ^ (toString right_expr (indent + 2))
				| And expressions => 
					withIndent indent $ "And\n" ^ List.foldl (fn (expr, acc) => acc ^ toString expr (indent + 2) ^ "\n") "" expressions
				| Or expressions =>
					withIndent indent $ "Or\n" ^ List.foldl (fn (expr, acc) => acc ^ toString expr (indent + 2) ^ "\n") "" expressions
				| Not expr =>
					withIndent indent "Not\n" ^ (toString expr (indent + 2))
	in
		(toString expr 0) ^ "\n"
	end;

fun valueOfVarible name [] = raise InvalidVariable
	| valueOfVarible name ((variable, value)::tail) =
		if variable = name then value 
		else valueOfVarible name tail;

valueOfVarible "a" [("a", true)] = true;
valueOfVarible "a" [("a1", true), ("a2", true), ("a", false)] = false;

fun eval variables expr =
	case expr of 
		True => true
		| False => false
		| Variable var => valueOfVarible var variables
		| Equiv (left_expr, right_expr) => eval variables left_expr = eval variables right_expr
		| Implies (left_expr, right_expr) => eval variables left_expr --> eval variables right_expr
		| And expressions => all_true expressions $ eval variables
		| Or expressions => exists_true expressions $ eval variables
		| Not expr => not $ eval variables expr


val variables = [("a", true), ("b", true), ("c", false)];
eval variables False = false;
eval variables True = true;
eval variables (Not True) = false;
eval variables (Not (Variable "a")) = false;
eval variables (Implies ((Variable "a"), (Variable "c"))) = false;
eval variables (Or [False, Variable "b", Not True]) = true;
eval variables (And [False, Variable "b", Not True]) = false;
eval variables (Or [False, Not (Variable "b"), Not (Not True)]) = true;

fun removeEmpty expr =
	case expr of 
		True => True
		| False => False
		| Variable var => Variable var
		| Equiv (left_expr, right_expr) => Equiv (removeEmpty left_expr, removeEmpty right_expr)
		| Implies (left_expr, right_expr) => Implies (removeEmpty left_expr, removeEmpty right_expr)
		| Not expr => Not $ removeEmpty expr
		| And [] => True (* ?? *)
		| And (h::[]) => removeEmpty h
		| And expressions => And $ map removeEmpty expressions
		| Or [] => True (* ?? *)
		| Or (h::[]) => removeEmpty h
		| Or expressions => Or $ map removeEmpty expressions;

eval variables (removeEmpty (And [])) = true;
eval variables (removeEmpty (And [And []])) = true;
eval variables (removeEmpty (And [And [], And []])) = true;
eval variables (removeEmpty (Or [])) = true;
eval variables (removeEmpty (And [True])) = true;
eval variables (removeEmpty (Or [False])) = false;

fun removeConstants expr =
	case expr of 
		True => True
		| False => False
		| Variable var => Variable var

		| Not True => False
		| Not False => True
		| Not expr => Not $ removeConstants expr

		| Equiv (left_expr, right_expr) => 
			let
				val left = removeConstants left_expr
				val right = removeConstants right_expr
			in
				case (left, right) of
					(True, True) => True
					| (False, False) => True
					| (True, False) => False
					| (False, True) => False
					| (e1, False) => Not e1
					| _ => Equiv (left, right)
			end

		| Implies (left_expr, right_expr) => 
			let
				val left = removeConstants left_expr
				val right = removeConstants right_expr
			in
				case (left, right) of
					(False, _) => True
					| (True, expression) => expression
					| _ => Implies (left_expr, right_expr)
			end

		| And expressions => 
			let
				val expressions = map removeConstants expressions
				val expression = removeEmpty $ And expressions
			in
				case expression of 
					And expressions => if contains expressions False then False
									   else removeEmpty $ And $ filter expressions (fn x => not (x = True))
					| x => x
			end
		| Or expressions => 
			let
				val expressions = map removeConstants expressions
				val expression = removeEmpty $ Or expressions
			in
				case expression of 
					Or expressions => if contains expressions True then True
									  else removeEmpty $ Or $ filter expressions (fn x => not (x = False))
					| x => x
			end;

removeConstants (Not True) = False;
removeConstants (Not False) = True;
removeConstants (And [True, True, True]) = True;
removeConstants (And [True, False, True]) = False;
removeConstants (And [True, True, Variable "a"]) = Variable "a";
removeConstants (And [True, True, Variable "a", False]) = False;
removeConstants (And [True, True, Variable "a", Variable "b"]) = And [Variable "a", Variable "b"];
removeConstants (Or [True, True, Variable "a"]) = True;
removeConstants (Or [False]) = False;
removeConstants (Or [False, Or [False, True]]) = True;
removeConstants (Or [False, Variable "a"]) = Variable "a";
removeConstants (Or [False, False, Variable "a", Variable "b"]) = Or [Variable "a", Variable "b"];
removeConstants (Implies (True, False)) = False;
removeConstants (Implies (True, True)) = True;
removeConstants (Implies (False, True)) = True;
removeConstants (Implies (False, False)) = True;
removeConstants (Implies (True, Variable "a")) = Variable "a";
removeConstants (Implies (False, Or [And [True, Not False], Variable "c"])) = True;
removeConstants (And [True, Not False]) = True;
removeConstants (Implies (True, Or [And [False, Not False], Variable "c"])) = Variable "c";
removeConstants (Implies (True, And [False, True])) = False;
removeConstants (Equiv (True, True)) = True;
removeConstants (Equiv (True, False)) = False;
removeConstants (Equiv (Implies (True, And [False, True]), False)) = True;
removeConstants (Not (Equiv (Variable 7, False)));

fun pushNegations expr =
	case expr of 
		True => True
		| False => False
		| Variable var =>  Variable var
		| Equiv (left_expr, right_expr) => Equiv (left_expr, right_expr)
		| Implies (left_expr, right_expr) => Implies (pushNegations left_expr, pushNegations right_expr)
		| And expressions => And $ map pushNegations expressions
		| Or expressions => Or $ map pushNegations expressions

		| Not (Not expression) => pushNegations expression
		| Not (Implies (left, right)) => Implies (Not $ pushNegations right, Not $ pushNegations left)
		| Not (Or expressions) => And $ map (fn x => pushNegations (Not x)) expressions
		| Not (And expressions) => Or $ map (fn x => pushNegations $ Not x) expressions
		| Not (Equiv (left, right)) => Equiv (pushNegations $ Not left, pushNegations right)
		| Not expression => Not $ pushNegations expression;

pushNegations (Not (Not (Not True))) = Not True;
pushNegations (Not (Not (Not (Variable "a")))) = Not (Variable "a");
pushNegations (Not (Implies (Not (Not (Variable "a")), Variable "b"))) = Implies (Not (Variable "b"), Not (Variable "a"));
pushNegations (Not (And [True, True, Variable "a"])) = Or [Not True, Not True, Not (Variable "a")];
pushNegations (Not (Or [True, True, Variable "a"])) = And [Not True, Not True, Not (Variable "a")];
pushNegations (Not (Equiv (True, Variable "a"))) = Equiv (Not True, Variable "a");

fun removeDuplicates [] = []
	| removeDuplicates (h::tail) = h::(removeDuplicates (filter tail (fn x => x <> h)));

removeDuplicates [1, 2, 1, 2, 1, 2] = [1, 2];

fun removeVars expr =
	case pushNegations expr of 
		True => True
		| False => False
		| Variable var => Variable var

		| Equiv (Variable l, Variable r) => if l = r then True else Equiv (Variable l, Variable r)
		| Equiv (Not (Variable l), Not (Variable r)) => if l = r then True else Equiv (Variable l, Variable r)
		| Equiv (left, right) => Equiv (removeVars left, removeVars right)

		| Implies (Variable l, Variable r) => if l = r then True else Implies (Variable l, Variable r)
		| Implies (Not (Variable l), Not (Variable r)) => if l = r then True else Implies (Variable l, Variable r)
		| Implies (left, right) => Implies (removeVars left, removeVars right)

		| And expressions => And $ removeDuplicates $ map removeVars expressions
		| Or expressions => Or $ removeDuplicates $ map removeVars expressions

		| Not expr => Not $ removeVars expr;

removeVars (Implies (Variable "a", Variable "a")) = True;
removeVars (Implies (Variable "a", Not $ Variable "a")) = Implies (Variable "a", Not $ Variable "a");
removeVars (Implies (Not $ Not $ Variable "a", Not $ Not $ Variable "a")) = True;
removeVars (Equiv (Variable "a", Variable "a")) = True;
removeVars (Equiv (Not (Variable "a"), Not (Variable "a"))) = True;
removeVars (Or [Variable "a", Variable "a", Not (Variable "b"), Not (Variable "b")]) = Or [Variable "a", Not (Variable "b")];

fun simplify expr =
	case removeEmpty o removeConstants o removeVars $ expr of 
		True => True
		| False => False
		| Variable var => Variable var

		| Equiv (Variable l, Variable r) => if l = r then True else Equiv (Variable l, Variable r)
		| Equiv (left, right) => Equiv (left, right)

		| Implies (Variable l, Not (Variable r)) => if l = r then Not $ Variable l else Implies (Variable l, Not (Variable r))
		| Implies (l, r) => Implies (l, r)

		| And ([Variable l, Not (Variable r)]) => if l = r then False else And [Variable l, Not $ Variable r]
		| And ([Variable l, Variable r]) => if l = r then Variable l else And [Variable l, Variable r]
		| And expressions => And expressions

		| Or ([Variable l, Not (Variable r)]) => if l = r then True else Or [Variable l, Not $ Variable r]
		| Or ([Variable l, Variable r]) => if l = r then Variable l else Or [Variable l, Variable r]
		| Or expressions => Or expressions

		| Not expr => Not expr;

simplify (Implies (Variable "a", Variable "a")) = True;
simplify (Implies (Variable "a", Not $ Variable "a")) = Not (Variable "a");
simplify (Implies (Not $ Not $ Variable "a", Not $ Not $ Variable "a")) = True;
simplify (Equiv (Variable "a", Variable "a")) = True;
simplify (Equiv (Not (Variable "a"), Not (Variable "a"))) = True;
simplify (Or [Variable "a", Variable "a", Not (Variable "b"), Not (Variable "b")]) = Or [Variable "a", Not $ Variable "b"];
simplify (Or [Variable "a", Not (Variable "a")]) = True;
simplify (And [Variable "a", Not (Variable "a")]) = False;
simplify (And [Variable "a", Variable "a"]) = Variable "a";
simplify (Implies (Variable "a", Not (Variable "a"))) = Not (Variable "a");
simplify (Equiv (Not $ Not (Variable "a"), Not $ Not $ Variable "a")) = True;
simplify (Not True) = False;
simplify (Not False) = True;

fun tseytinTransformation variables expr =
	let
		(* 1st substitute variables *)
		fun substitute variables expr = 
		let
			fun substitute (hd::tail) expr =
				(case expr of 
					True => (True, hd::tail)
					| False => (False, hd::tail)
					| Variable var => if contains (hd::tail) var then raise InvalidVariable
									  else (Variable var, hd::tail)
					| Equiv (left, right) => 
						let
							val (l, remaining) = substitute tail left
							val (r, remaining) = substitute remaining right
						in
							(Equiv (Variable hd, Equiv (l, r)), remaining)
						end
					| Implies (left, right) => 
						let
							val (l, remaining) = substitute tail left
							val (r, remaining) = substitute remaining right
						in
							(Equiv (Variable hd, Implies (l, r)), remaining)
						end
					| And expressions => 
						let
							val (expressions, tail) = List.foldr (fn (tree, (acc, variables)) => 
										let
											val (changedTree, remainingVariables) = substitute variables tree
										in
											(changedTree::acc, remainingVariables)
										end) 
									 ([], variables)
									 expressions
							(*val (expressions, tail) = kleisliMap substitute tail expressions*)
						in
							(Equiv (Variable hd, And expressions), tail)
						end
					| Or expressions => 
						let
							val (expressions, tail) = List.foldr (fn (tree, (acc, variables)) => 
																	let
																		val (changedTree, remainingVariables) = substitute variables tree
																	in
																		(changedTree::acc, remainingVariables)
																	end) 
																 ([], variables)
																 expressions
							(*val (expressions, tail) = kleisliMap substitute tail expressions*)
						in
							(Equiv (Variable hd, Or expressions), tail)
						end
					| Not expr => 
						let
							val (e, remaining) = substitute tail expr
						in
							(Equiv (Variable hd, Not e), remaining)
						end)
				| substitute [] expr =
					case expr of
						Or _ => raise InsufficientVariables
						| And _ => raise InsufficientVariables
						| Implies _ => raise InsufficientVariables
						| Equiv _ => raise InsufficientVariables
						| Not _ => raise InsufficientVariables
						| e => (e, [])
			val (res, _) = substitute variables expr
		in
			res
		end
		(* 2nd convert to equivalence conjunction *)
		fun toArray acc expr =
			case expr of 
				True => (True, acc)
				| False => (False, acc)
				| Variable var => (Variable var, acc)
				| Equiv (Variable v1, right_expr) => 
					let
					in
						if contains variables v1 then 
							let
								val (name, acc) = toArray acc right_expr
								val expr = Equiv (Variable v1, name)
							in
								(Variable v1, expr::acc)
							end
						else toArray acc right_expr (* not sure about this *)
					end
				| Equiv (left_expr, right_expr) => 
					let
						val (left, acc) = toArray acc left_expr 
						val (right, acc) = toArray acc right_expr
					in
						(Equiv (left, right), acc)
					end
				| Implies (left_expr, right_expr) => 
					let
						val (left, acc) = toArray acc left_expr 
						val (right, acc) = toArray acc right_expr
					in
						(Implies (left, right), acc)
					end
				| And expressions =>
					let
						val (names, acc) = kleisliMap toArray acc expressions
					in
						(And names, acc)
					end
				| Or expressions => 
					let
						val (names, acc) = kleisliMap toArray acc expressions
					in
						(And names, acc)
					end
				| Not expr => 
					let
						val (name, acc) = toArray acc expr
					in
						(Not name, acc)
					end
		fun transform tree (*vars ne rabiš pošiljat not*) acc =
			case tree of
				Equiv (Variable a, e) => 
					let
					 	val (name, acc) = transform e acc
					 in
					 	(Variable a, (Equiv (Variable a, name))::acc)
					 end 
				| Equiv (l, r) => 
					let
						val (left, acc) = transform l acc 
						val (right, acc) = transform r acc
					in
						(Equiv (left, right), acc)
					end
				|  ... 
				| Not e => 
					let 
						val (name, acc) = transform e acc
					in
						(Not name, acc)
					end 
				| True => True
				| False => False
				| Variable a => Variable a

		(* 3rd convert to CNF *)
		fun transformTree expr =
			case expr of 
				True => True
				| False => False
				| Variable var => Variable var

				| Equiv (Variable v1, Not (Variable v2)) => And [Or [Not (Variable v1), Not (Variable v2)], 
																 Or [Variable v1, Variable v2]]
				| Equiv (Variable v1, And [Variable v2, Variable v3]) => And [Or [Not (Variable v2), Not (Variable v3), Variable v1], 
																			  Or [Variable v2, Not (Variable v1)], 
																			  Or [Variable v3, Not (Variable v1)]]
				| Equiv (Variable v1, Or [Variable v2, Variable v3]) => And [Or [Variable v2, Variable v3, Not (Variable v1)],
																			 Or [Not(Variable v2), Variable v1], 
																			 Or [Not (Variable v3), Variable v1]]
				| Equiv (Variable v1, Implies (Variable v2, Variable v3)) => And [Or [Not (Variable v2), Variable v3, Not (Variable v1)],
																			      Or [Variable v2, Variable v1], 
																				  Or [Not (Variable v3), Variable v1]]
				| Equiv (Variable v1, Equiv (Variable v2, Variable v3)) => And [Or [Not (Variable v2), Not (Variable v3), Variable v1],
																				Or [Not (Variable v2), Variable v3, Not (Variable v1)],
																				Or [Variable v2, Not (Variable v3), Not (Variable v1)],
																				Or [Variable v2, Variable v3, Variable v1]]
				
				| Equiv (left_expr, right_expr) => Equiv (transformTree left_expr, transformTree right_expr)
				| Implies (left_expr, right_expr) => Implies (transformTree left_expr, transformTree right_expr)
				| And expressions => And $ map transformTree expressions
				| Or expressions => Or $ map transformTree expressions
				| Not expr => Not $ transformTree expr
		val tree = substitute variables expr
		
		(*val _ = print $ toString $ tree
		val _ = print $ "---\n"*)
		
		val (name, expressions) = toArray [] tree
		val expressions = name::expressions

		(*val _ = print $ toString $ And expressions
		val _ = print $ "---\n"*)
	in
		removeConstants $ transformTree $ And expressions
		(*tree*)
	end;

tseytinTransformation    ["x4", "x3", "x2", "x1"] 
											(Implies 
												(And 
													[Or 
														[Variable "p", 
														 Variable "q"], 
													Variable "r"],
												Not (Variable "s")));

(* for debugging purposes *)
(* converts (string * bool) list to string *)
fun varsToString ((v, true)::tail) = "(" ^ v ^ ", true), " ^ (varsToString tail)
	| varsToString ((v, false)::tail) = "(" ^ v ^ ", false), " ^ (varsToString tail)
	| varsToString [] = "";

fun buildTree vars expr =
	case expr of
		Variable a => (Variable a, vars)
		| True => (True, vars)
		| False => (False, vars)
		| Or expressions => 
							let
								val _ = print("Start And")
								val (expressions, tail) = List.foldr (fn (tree, (acc, vars)) => 
									let
										val (changedTree, remainingVariables) = buildTree vars tree
									in
										(changedTree::acc, remainingVariables)
									end) 
								 ([], vars)
								 expressions
							in
								(Equiv (Variable (hd vars), Or expressions), tail)
							end
		| Not a => let
						val _ = print("Not \n")
						val variable = hd vars
						val remainingVars = tl vars
						val (rvL, exL) = buildTree remainingVars a
					in
						(Equiv (Variable variable, Not a), remainingVars)
					end
		| And expressions => 
					let
						val (expressions, tail) = List.foldr (fn (tree, (acc, vars)) => 
							let
								val (changedTree, remainingVariables) = buildTree vars tree
							in
								(changedTree::acc, remainingVariables)
							end) 
						 ([], vars)
						 expressions
					in
						(Equiv (Variable hd, And expressions), tail)
					end;

(*buildTree ["x4", "x3"] (And []);*)

fun SATsolver variables expr =
	let
		fun isVariableValid variable value variables = valueOfVarible variable variables = value
		handle InvalidVariable => true (* not yet 'removed' *)

		fun safeValueOf ((name, value)::tail) variable = if name = variable then SOME value 
														 else safeValueOf tail variable
			| safeValueOf [] _ = NONE

		(* prune single variables *)
		fun findSingleVariables variables expr =
			case expr of 
				Or [Variable v] => if isVariableValid v true variables then (Or [True], (v, true)::variables)
								   else raise InvalidVariable
			   	| Or [Not (Variable v)] => if isVariableValid v false variables then (Or [Not False], (v, false)::variables)
								   		   else raise InvalidVariable 
				| Variable var => (case safeValueOf variables var of
					SOME true => (True, variables)
					| SOME false => (False, variables)
					| NONE => (Variable var, variables))

				| True => (True, variables)
				| False => (False, variables)
				| Equiv (left_expr, right_expr) => 
					let
						val (left, variables) = findSingleVariables variables left_expr
						val (right, variables) = findSingleVariables variables right_expr
					in
						(Equiv (left, right), variables)
					end
				| Implies (left_expr, right_expr) =>
					let
						val (left, variables) = findSingleVariables variables left_expr
						val (right, variables) = findSingleVariables variables right_expr
					in
						(Equiv (left, right), variables)
					end
				| And expressions => 
					let
						val (expressions, variables) = kleisliMap findSingleVariables variables expressions
					in
						(And expressions, variables)
					end
				| Or expressions =>
					let
						val (expressions, variables) = kleisliMap findSingleVariables variables expressions
					in
						(Or expressions, variables)
					end
				| Not expr =>
					let
						val (expr, variables) = findSingleVariables variables expr
					in
						(Not expr, variables)
					end

		fun selectVariable variables expr =
			case expr of 
				Equiv _ => NONE
				| Implies _ => NONE
				| And [] => NONE
				| Or [] => NONE
				| True => NONE
				| False => NONE

				| And expressions => 
					let
						(* can optimize by only taking first *)
						val variables = compactMap $ map (selectVariable variables) expressions
					in
						case variables of
							h::_ => SOME h
							| [] => NONE
					end
				| Or expressions => 
					let
						(* can optimize by only taking first *)
						val variables = compactMap $ map (selectVariable variables) expressions
					in
						case variables of
							h::_ => SOME h
							| [] => NONE
					end
				| Variable var => (case safeValueOf variables var of
					SOME _ => NONE
					| NONE => SOME var)
				| Not expr => selectVariable variables expr

		val (updatedExpr, variables) = findSingleVariables variables expr
		val simplified = simplify updatedExpr

(*		val _ = print $ toString expr
		val _ = print $ "\n"
		val _ = print $ toString $ updatedExpr
		val _ = print $ "\n"
		val _ = print $ varsToString $ variables
		val _ = print $ "\n"
		val _ = print $ toString $ simplified
		val _ = print $ "\n"*)
	in
		case simplified of 
			True => SOME variables
			| And [] => SOME variables
			| False => NONE
			| Or [] => NONE
			| _ => 
				let
					val var = selectVariable variables simplified
				in
					(* why this doesn't work ???
					case Option.map (fn var => SATsolver ((var, true)::variables) simplified) var of 
						SOME vars => vars
						| NONE => (case Option.map (fn var => SATsolver ((var, false)::variables) simplified) var of
							SOME vars => vars
							| NONE => NONE)*)

					(* this works just fine
					case var of 
						SOME var => (case SATsolver ((var, true)::variables) simplified of
							SOME vars => SOME vars
							| NONE => (case SATsolver ((var, false)::variables) simplified of
								SOME vars => SOME vars
								| NONE => NONE))
						| NONE => NONE*)

					(* this seems the nicest *)
					case var of 
						SOME var => mapNone (fn () => (SATsolver ((var, false)::variables) simplified))
											(SATsolver ((var, true)::variables) simplified)
						| NONE => NONE
				end
	end
	handle InvalidVariable => NONE;

SATsolver [] (And [Or [Variable "x1"], 
				Or [Not $ Variable "x2"], 
				Or [Variable "x1", Variable "x2"]]) = SOME [("x2", false), ("x1", true)];
SATsolver [] (And [Or [Variable "x1"], 
				Or [Variable "x1"]]) = SOME [("x1", true), ("x1", true)];
SATsolver [] (And [Or [Variable "x1", Variable "x2"], 
				Or [Not $ Variable "x1"],
				Or [Not $ Variable "x2"]]) = NONE;
(* failing test
SATsolver [] (And [Or [Variable "x1"], Or [Not $ Variable "x1"]]);
*)

(*val expr = tseytinTransformation 
			["x4", "x3", "x2", "x1"] 
			(Implies 
				(And 
					[Or 
						[Variable "p", 
						 Variable "q"], 
					Variable "r"],
				Not (Variable "s")));
val variables = SATsolver [] expr;
eval (valOf variables) expr = true;*)

(* expects `e1` and `e2` already converted into CNF *)
(*fun equivalentExpressions e1 e2 = case (SATsolver [] e1, SATsolver [] e2) of
	(SOME v1, SOME v2) => (eval v1 e1) = (eval v2 e2)
	| _ => false;

equivalentExpressions expr (And [Or [Variable "x1"], 
								 Or [Not $ Variable "x2"], 
								 Or [Variable "x1", Variable "x2"]]) = true;
equivalentExpressions expr (And [Or [Variable "x1", Variable "x2"],
							     Or [Not $ Variable "x1"],
							     Or [Not $ Variable "x2"]]) = false;*)

(* 2nd Part *)

type timetable = {day: string, time: int, course: string} list;
type student = {id: int, curiculum: string list};
type variable = {s: int, c: string, t: (string * int), p: int};

val timetable = [{day = "torek", time = 7, course = "DS"},
	     		 {day = "sreda", time = 10, course = "DS"},
		 		 {day = "petek", time = 14, course = "DS"},
		 		 {day = "torek", time = 7, course = "P2"},
				 {day = "ponedeljek", time = 12, course = "P2"}]: timetable;

val students = [{id = 63170000, curiculum = ["DS", "P2", "OPB", "RK"]}, 
 				{id = 63160000, curiculum = ["P2", "DS", "ARS"]}]: student list

(*-------*)

fun generateVariables timetable students =
	let
		fun generateCuriculum timetable student =
			let
				fun generatePlaces {s=id, c=course, t=time} p acc = 
					if p = 0 then acc 
					else generatePlaces {s=id, c=course, t=time} (p - 1) (Variable {s=id, c=course, t=time, p=p}::acc)

				fun generateSubjects ({day=day, time=time, course=course}::timetable) id subject acc =
					let
						val numberOfSeats = 3 (* can be configured *)
						val places = generatePlaces {s = id, c = course, t = (day, time)} numberOfSeats []
						val acc = if course = subject then places::acc
								  else acc
					in
						generateSubjects timetable id subject acc
					end
					| generateSubjects [] _ _ acc = acc;

				fun generateCuriculum timetable {id=id, curiculum=(subject::curiculum)} acc = 
					let
						val subjects = generateSubjects timetable id subject []
					in
						generateCuriculum timetable {id=id, curiculum=curiculum} (subjects::acc)
					end
					| generateCuriculum _ _ acc = acc

				(*fun groupBySubjects [] acc = acc
					| groupBySubjects (variable::tail) [] =
						groupBySubjects tail [[variable]]
					| groupBySubjects ((Variable {s=s, c=c, t=t, p=p})::tail) (((Variable {s=s2, c=c2, t=t2, p=p2})::acc)::acc2) =
						if c = c2 then groupBySubjects tail ((Variable {s=s, c=c, t=t, p=p})::(Variable {s=s2, c=c2, t=t2, p=p2})::acc)::acc2)
						else groupBySubjects tail ((Variable {s=s, c=c, t=t, p=p})::(Variable {s=s2, c=c2, t=t2, p=p2})::acc)*)
					
			in
				(flatMap id o flatMap id) $ generateCuriculum timetable student []
			end

		fun generateVariables timetable [] acc = acc
			| generateVariables timetable (student::tail) acc = 
				let
					val curiculum = generateCuriculum timetable student
				in
					generateVariables timetable tail (curiculum::acc)
				end
	in
		generateVariables timetable students []
	end;

generateVariables timetable students;

fun generateLogicalExpressions variables =
	let
(*		fun generateOrExpressions [] expr = expr
			 | generateOrExpressions ((Variable {s=s, c=c, t=t, p=p})::tail) (And [Or []]) = 
			 	generateOrExpressions tail (And [Or ()])
					if prev = cur then expressionsForStudent tail ((Variable current)::acc)
					else expressionsForStudent tail ((Variable current)::acc)
				end
			| expressionsForStudent (variable::tail) acc = 
				expressionsForStudent tail ((Variable variable)::acc)*)

		fun generateLogicalExpressions [] acc = acc
			| generateLogicalExpressions (student::tail) acc = 
				let
					(*val expressions = expressionsForStudent student []*)
				in
					(*generateLogicalExpressions tail (expressions::acc)*)
					[]
				end
	in
		generateLogicalExpressions variables []
	end;

generateLogicalExpressions $ generateVariables timetable students;

(*fun problemReduction timetable students =*)


OS.Process.exit(OS.Process.success);