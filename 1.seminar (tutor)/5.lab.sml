(* Implementirajte sledeči programski vmesnik za delo z
 * racionalnimi števili. Vaša struktura naj se imenuje
 * Rational in naj definira podatkovni tip (datatype) rational,
 * ki je lahko bodisi celo število (Whole) bodisi ulomek (Frac).
 *
 * Ulomki naj sledijo naslednjim pravilom:
 *   1. Imenovalec je vedno strogo večji od 1
 *   2. Vsak ulomek je okrajšan
 *
 * V oddani datoteki implementirajte le strukturo Rational :> RATIONAL!
 * Z drugimi besedami, vaša datoteka naj ne vsebuje podpisa RATIONAL.
 *)

infixr 0 $ (* https://typeclasses.com/featured/dollar *)
fun f $ arg = f arg;

signature RATIONAL =
sig
    (* Definirajte podatkovni tip rational,
       ki podpira preverjanje enakosti. *)
    eqtype rational

    (* Definirajte izjemo, ki se kliče pri
       delu z neveljavnimi ulomki. *)
    exception BadRational

    (* Vrne racionalno število, ki je rezultat
       deljenja dveh podanih celih števil. *)
    val makeRational: int * int -> rational

    (* Vrne nasprotno vrednost podanega števila. *)
    val neg: rational -> rational

    (* Vrne obratno vrednost podanega števila. *)
    val inv: rational -> rational

    (* Funkcije za seštevanje, odštevanje, množenje in deljenje.
       Rezultat vsake operacije naj sledi postavljenim pravilom. *)
    val add: rational * rational -> rational
    val sub: rational * rational -> rational
    val mul: rational * rational -> rational
    val dvd: rational * rational -> rational

    (* Vrne niz, ki ustreza podanemu številu.
       Če je število celo, naj vrne niz oblike "x" oz. "~x".
       Če je število ulomek, naj vrne niz oblike "x/y" oz. "~x/y". *)
    val toString: rational -> string
end;


structure Rational :> RATIONAL =
struct
    datatype rational = 
      Whole of int
      | Frac of int*int

    exception BadRational

    fun gcd x 0 = x
      | gcd x y = gcd y (x mod y)

    (* Vrne racionalno število, ki je rezultat
    deljenja dveh podanih celih števil. *)
    fun makeRational(x, y) = if x mod y = 0 then Whole (x div y)
                             else Frac (x div (gcd x y), y div (gcd x y))

    (* Vrne nasprotno vrednost podanega števila. *)
    fun neg (Whole x) = Whole $ ~x
      | neg (Frac (x, y)) = makeRational(~x, y)

    (* Vrne obratno vrednost podanega števila. *)
    fun inv (Whole x) = Frac (1, x)
      | inv (Frac (x, y)) = Frac (y, x)

    (* Funkcije za seštevanje, odštevanje, množenje in deljenje.
       Rezultat vsake operacije naj sledi postavljenim pravilom. *)
    fun add (Whole x, Whole y) = Whole (x + y)
      | add (Frac (x1, y1), Frac (x2, y2)) = makeRational (x1 * y2 + x2 * y1, y1 * y2)
      | add (Whole x, Frac (x2, y2)) = makeRational (x * y2 + x2, y2)
      | add (Frac (x1, y1), Whole y) = makeRational (y1 * y + x1, y1)
    
    fun sub(x, y) = add (x, neg y)

    fun mul (Whole x, Whole y) = Whole (x * y)
      | mul (Frac (x1, y1), Frac (x2, y2)) = makeRational (x1 * x2, y1 * y2)
      | mul (Whole x, Frac (x2, y2)) = makeRational (x * x2, y2)
      | mul (Frac (x1, y1), Whole y) = makeRational (y * x1, y1)

    fun dvd(x, y) = mul (x, inv y)

    (* Vrne niz, ki ustreza podanemu številu.
       Če je število celo, naj vrne niz oblike "x" oz. "~x".
       Če je število ulomek, naj vrne niz oblike "x/y" oz. "~x/y". *)
    fun toString (Whole x) = Int.toString x
      | toString (Frac (x, y)) = (Int.toString x) ^ "/" ^ (Int.toString y)
end;

Rational.toString $ Rational.makeRational (10, 20);
Rational.toString $ Rational.makeRational (20, 10);
Rational.toString $ Rational.makeRational (~1, 2);
Rational.toString $ Rational.makeRational (3, ~9);

Rational.toString $ Rational.neg $ Rational.makeRational (3, ~1);
Rational.toString $ Rational.neg $ Rational.makeRational (3, 1);
Rational.toString $ Rational.inv $ Rational.makeRational (3, 1);
Rational.toString $ Rational.inv $ Rational.makeRational (1, 3);

Rational.toString $ Rational.add (Rational.makeRational (2, 1), Rational.makeRational (5, 1));
Rational.toString $ Rational.add (Rational.makeRational (1, 3), Rational.makeRational (1, 4));
Rational.toString $ Rational.add (Rational.makeRational (1, 3), Rational.makeRational (4, 1));
Rational.toString $ Rational.add (Rational.makeRational (3, 1), Rational.makeRational (1, 4));

Rational.toString $ Rational.sub (Rational.makeRational (2, 1), Rational.makeRational (5, 1));
Rational.toString $ Rational.sub (Rational.makeRational (1, 3), Rational.makeRational (1, 4));
Rational.toString $ Rational.sub (Rational.makeRational (1, 3), Rational.makeRational (4, 1));
Rational.toString $ Rational.sub (Rational.makeRational (3, 1), Rational.makeRational (1, 4));

Rational.toString $ Rational.mul (Rational.makeRational (2, 1), Rational.makeRational (5, 1));
Rational.toString $ Rational.mul (Rational.makeRational (1, 3), Rational.makeRational (1, 4));
Rational.toString $ Rational.mul (Rational.makeRational (1, 3), Rational.makeRational (4, 1));
Rational.toString $ Rational.mul (Rational.makeRational (3, 1), Rational.makeRational (1, 4));

Rational.toString $ Rational.dvd (Rational.makeRational (2, 1), Rational.makeRational (5, 1));
Rational.toString $ Rational.dvd (Rational.makeRational (1, 3), Rational.makeRational (1, 4));
Rational.toString $ Rational.dvd (Rational.makeRational (1, 3), Rational.makeRational (4, 1));
Rational.toString $ Rational.dvd (Rational.makeRational (3, 1), Rational.makeRational (1, 4));

val _ = OS.Process.exit(OS.Process.success);